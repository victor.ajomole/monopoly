# Titel pagina

* Opdracht : Data Analysis
* Spel     : Monopoly Terminal Game

# Domain Model

## Stap 1 - verzamel alle kandidaat zelfstandige naamwoorden

Lees [monopoly-project-description-NL.md](input/monopoly-project-description-NL.md)

### input : Spelbeschrijving

... is een bord**spel**  ... 

... meerdere **speler**s 

... om de **beurt** zal 

... zijn **pion** op het **bord**

... op de **plaats** waar ze landt

... een reeks **actie**s  uitvoeren

... zo kan ze **eigendommen** aankopen ...

... en daarop een aantal **huizen** bouwen ...

... de **eigenaar** een **huur** moeten betalen ...

### input : Monopoly spelregels

... door **straten** te kopen ...

... huizen of **hotel** 

... al het **geld** in het spel naar jou toe

### input : 1. Een potje monopoly starten
... gooit de twee **dobbelstenen**

### input : 2. Een beurt spelen
... Als je landt op een **bezitting** (een **straat**, **station**s of **nutsbedrijf**) die nog 

... op het **vak**je “naar de **gevangenis**”

... Dit kan ook gebeuren als je een **Algemeen Fonds**- of **Kans**kaart trekt

... ontvang je geen **salaris**.

... Tenslotte kun je drie **beurt**en

_Ter info : we nemen niet alle details over alle vakjes op in de analyse_ 

... Ook dit staat aangegeven op het **eigendomsbewijs**.

### input : 3. Huizen en hotels bouwen

... alle straten van een **stad** (kleur) bezit.

... (straat die) bezwaard is met een **hypotheek**. 

### input : PROGRAMMAVEREISTEN

... Elke speler geeft ...  zijn **naam** in.

... **witte** pion ... en **zwarte** pion 

... vorige speler max. 24u **bedenktijd** om hun

... na elk spel wordt de **uitkomst** automatisch de **namen** van de spelers, een DateTime **stamp** wanneer het spel gespeeld is en de uiteindelijke **winnaar**(of draw).

... Ook wordt er bijgehouden **hoe lang** dit spel geduurd heeft, **hoeveel ronden** er zijn gespeeld, en **hoeveel geld** iedere speler had op het einde.

... Deze **lijst** met **gespeelde spel**

### input : UITBREIDINGEN
_**Opgelet** : Ook al is het niet zeker of de uitbreidingen geimplementeerd worden, zullen we deze toch meepakken in de data analyse_  

... mogelijk een **highscore**s tabel ... **hoeveel keer** elke speler al is gewonnen ...  diens **ELO-rating**  


## Stap 2A - zet de zelfstandige naamworden om in klass/attribuutn of associaties   

| Zelfstandig naamwoord              | EN                                                   | Type                   | commentaar                                                                                                                                                                              |
|:-----------------------------------|------------------------------------------------------|:-----------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **spel**                           | GameSession                                          | class                  | Eens dat de applicatie gestart is, kan ofwel spel starten, ofwel voorgaande spelen of highscore  zien.  We veronderstellen dus ook dat er een aparte klasse MonopolyApplicatie is.      |
| **MonopolyApplicatie**             | MonopolyApplication                                  | class                  | zie hierboven : De applicatie maakt een spel, en start dan enkel een spel wanneer erom gevraagd wordt.                                                                                  |
| **speler**                         | Player                                               | class                  | diens naam moet ingegeven worden                                                                                                                                                        |
| **beurt**                          | Turn                                                 | class or attribuut     | just a number? -> attribuut, or is it more than that?                                                                                                                                   |
| **pion**                           | Pawn                                                 | class                  | hoort bij een speler, staat op het bord, kan worden verplaatst: het is waarschijnlijk een klasse, want het heeft ook een attribuut kleur                                                |
| **bord**                           | Board                                                | class                  | kan niet als een primitief datatype (int, float,string, date, .) worden gemodelleerd, en is niet helemaal hetzelfde als een 'spel'                                                      |
| **plaats**                         | position                                             | ~~class or~~ attribuut | een nummer tussen 0 en 40 en attribuut voor de pion                                                                                                                                     |
| **actie**s                         |                                                      |                        | just a text? -> attribuut of is dit meer.  Voor de eenvoud laten we dit  voorlopig uit het Domain Model                                                                                 |
| **eigendom**men                    | Property                                             | class                  | is dit een apart gegeven, of is dit een soort 'plaats' waar je op kan landen?. Nee er is meer - een speler kan eigendom bezitten, er is een naam en een kleur                           | 
| **huizen**                         | house,Housing                                        | ~~class or~~ attribuut | lijkt een klassse, maar het kan ook een integer attribuut van eigendom zijn (0, 1,2,3,4 of 5 "hotel"). Een enumuration is ideaal wanneer we met een beperkt aantal vaste waarden werken |
| **eigenaar** of  _is eigendom van_ | owns                                                 | associatie             | commerciele regel : Iedere Speler kan 0 tot veel Eigendommen hebben, Ieder eigendom hoort bij 0 of 1 Speler                                                                             |
| **huur**                           | /rent                                                | (berekend) attribuut   | dit is een (berekend) attribuut van eigendom,  en is afhankelijk van  aantal huizen op de eigendom _(of het aantal stations/nutsbedrijven in bezit)_                                    |
| **straat**                         |                                                      | _synoniem_             | dit is hetzelfde als een eigendom                                                                                                                                                       |
| **hotel**                          | HOTEL                                                | attribuut **waarde**   | is een van de mogelijke waarden voor 'aantal huizen' - nl. 5  - het feit dat dit daarna anders gevisualiseerd wordt (1 rood gebouw) is een UI probleem                                  |
| **geld**                           | money                                                | attribuut              | een attribuut voor de speler, gewoon een float is voldoende                                                                                                                             |
| **dobbelsteen**                    | Die                                                  | class ~~or attribuut~~ | het heeft een mogelijke waarde, we hebben er 2 nodig, en je moet ze kunnen rollen (ie. methode hebben)                                                                                  |
| **bezitting**                      |                                                      | _synoniem_             | zelfde als eigendom - er zijn blijkbaar 3 soorten (subclass?) :  (een straat, stations of nutsbedrijf)                                                                                  | 
| **vak**                            | Square                                               | class                  | er zijn 40 vakjes op het bord. Het kan een Eigendom of  Start of Gevangenis of  .. zijn<br/> We kunnen ook de de 'plaats' als attribuut gebruiken. De pion STAAT dan op een vak         |
| **staan op**                       | is positioned on                                     | associatie             | ipv. plaats een attribuut te maken van **Pion** en **Vak**. Kan dit ook via associatie. Pion met kleur 'wit', staat op Vak met plaats 33.                                               | 
| **gevangenis**                     | JAIL                                                 | attribuut **waarde**   | de waarde voor de 'naam' van een vak, andere opties zijn straatnamen, vrij parkeren etc..                                                                                               | 
| **Algemeen Fonds en Kans**         | CHANCE / CHEST                                       | attribuut **waarde**   | de waarde voor de 'naam' van een vak, andere opties zijn straatnamen, vrij parkeren etc..                                                                                               | 
| **kaart**                          |                                                      |                        | Alle info op een 'kaart' is eigenlijk info die bij een vakje hoor. We make hier geen extra klasse van.                                                                                  | 
| **salaris**                        | salary                                               | attribuut              | Dit is een attribuut van het "GO" vakje, en geeft aan hoeveel je krijgt bijo passeren. Kan ook een CONSTANTE zijn van het spel                                                          | 
| **eigendomsbewijs**                |                                                      | _synoniem_             | Het eigendomsbewijs hebben = Het vakje/eigendom is in handen van Speler x                                                                                                               | 
| **stad / kleur**                   | colour,PropertyColor                                 | attribuut              | attribuut voor eigendom - kan text zijn, of misschien zelfs beter een Enumeratie                                                                                                        | 
| **hypotheek**                      | mortgage                                             | attribuut              | attribuut voor eigendom - ofwel is de straat in hypotheek, ofwel niet --> boolean                                                                                                       | 
| **naam** van speler                | name                                                 | attribuut              | -> speler.naam : string                                                                                                                                                                 |
| **witte/zwarte** pion              | WHITE,BLACK, PawnColor                               | attribuut **waarde**n  | ofwel een string, ofwel een enumeratie (het kan maar 2 waarden hebben)                                                                                                                  |
| **bedenktijd**                     | maxTurnDuration                                      | attribuut              | we wisten nog niet of 'beurt' een attribuut was - Nu weten we dat het een klasse is, met een soort bedenktijd - en waarschijnlijk een starttijd?                                        |
| **uitkomst**                       | Outcome  (playerName,</br>timestamp, </br>winner     | class & attributen     | in dezelfde alinea staan alle attributen : spelersnaam, timestamp, winnaar - die uitkomst hoort bij elke spel, maar ook speler. Hoe doen we dat?                                        |
| **hoe lang ..**                    | duration,</br>numberOfRound, </br>finalAmountOfMoney | attributen             | nog attributen van uitkomst : duration, numberOfRound, finalAmountOfMoney                                                                                                               |
| **hoeveel ronden**                 | Round                                                | class                  | Ofwel delen we het aantal 'beurten' door 2 - dus ergens aantal beurten bijhouden. Ofwel houden we apart Ronde bij, Een ronde heef beurten..                                             |
| **lijst**                          |                                                      |                        | lijsten of rapporteren of  overzichten zijn geen conceptuele klassen, ze kunnen opgebouwd worden uit reeds bestaande informatie                                                         |
| **gespeelde spel**                 |                                                      | _synoniem_             | is hetzelfde als 'uitkomst'                                                                                                                                                             |
| **highscore**                      | /numberOfWins                                        | (berekend) attribuut   | als we alle 'uitkomsten' hebben, dan kunnen we berekenen hoeveel keer een speler heeft gewonnen. We voegen dit attribuut toe bij de speler.                                             |
| **ELO-rating**                     | /ELO                                                 | (berekend) attribuut   | als we alle 'uitkomsten' hebben, dan kunnen we berekenen wat de ELO is. We voegen dit attribuut toe bij de speler.                                                                      |
| **account**                        | Account                                              | class                  | De highscore en ELO lijken bij speler te horen, maar deze 2 atributen bestaan ook wanneer een speler GEEN spel aan het spelen is, we houden deze 2 gegevens dus in apart klasse bij     |



## Step 2B - consolidate classes and attributen
``` plantuml
@startuml
skinparam style strictuml

' om duration bij te houden hebben we start en eind tijd nodig                  
Class GameSession{
  startDate
  endDate
  maxTurnDuration
}                                    

class MonopolyApplication{
  main()
}
                   
Class Player{
 name
 money
}
                                 
Class Turn{
  turnNumber
  startDateTime
  endDateTime
  /duration 
}                                    

Class Pawn{
 /position
 color:PawnColor
}   

enum PawnColor{
  Black
  White
}

Class Board{
}
          
Class Property{
 name
 color:StreetColor 
 housing:Housing
 mortgage:boolean
 mortgageAmount:float
 price
 /rent
}
                  
'Player "0..1" - "*" Property : owns >
                      
enum StreetColor{
  Green
  Dark Blue
  Light Blue
  Red Set
  Yellow
  Orange
  Brown
  Purple
}

enum Housing{
  0: "No housing"
  1: "1 House"
  2: "2 Houses"
  3: "3 Houses"
  4: "4 Houses"
  5: "Hotel"
}

Class Die{
  faceValue
  roll()
}                                    
      
' Generalisation of a property
Class Square{
 name
 position
}

'Pawn "0..1" - "1" Square : is positioned on >

' specialisation of Property
Class Lot{}
Class Railroad{}
Class Utility{}

' other special(isation) Square
Class Go{
    salary
}

'We moeten de Outcome opsplitsen in een Spel specifiek gedeelte, en een gedeelte met info over speler resultaat

Class GameOutcome{
  timeStamp
  winner:String
  duration
  numberOfRounds:int
}

Class PlayerOutcome{
  playerName:String
  finalAmountOfMoney:float      
}

Class Account{
  name:String
  /numberOfWins
  /ELO-rating
}
@enduml
```

## Step 3 - adding the associaties + multiplicities 

``` plantuml
@startuml
skinparam style strictuml

MonopolyApplication "1" -- "0..1" GameSession : launches >
MonopolyApplication "1" -l- "*" Account : keeps track of >
GameSession "1" -- "2" Die : played with > 
GameSession "1" -- "1" Board : played on >
Player "2..8" -r- "*" GameSession : plays >
GameSession "1" -- "0..1" GameOutcome : records >
(Player, GameSession) .. PlayerOutcome  
Player "1" -u- "*" Turn : takes >
Die -[hidden]- Board
Board "1" -- "40" Square : consists of >
Player "1" -- "1" Pawn : plays with >
Pawn "0..8" -- "1" Square: lands >
Player "0..1" -- "*" Property : owns >
Lot -u-|> Property
Railroad -u-|> Property
Utility -u-|> Property
Property -u-|> Square
Go -r-|> Square
IncomeTax -l-|> Square
@enduml
```

## Step 4A - Bring it all together  

``` plantuml
@startuml
skinparam style strictuml

' om duration bij te houden hebben we start en eind tijd nodig                  
Class GameSession{
  startDate:Date
  endDate:Date
  maxTurnDuration:int
}                                    

class MonopolyApplication{
  main()
}
                   
Class Player{
 name:String
 money:float 
}
                                 
Class Turn{
  turnNumber
  startDateTime
  endDateTime
  /duration 
}                                    

Class Pawn{
 /position:int
 color:PawnColor
}   

enum PawnColor{
  Black
  White
}

Class Board{ 
}
          
Class Property{
 color:StreetColor 
 housing:Housing
 mortgage:boolean
 mortgageAmount:float
 price
 /rent
}
                  
'Player "0..1" - "*" Property : owns >
                      
enum StreetColor{
  Green
  Dark Blue
  Light Blue
  Red Set
  Yellow
  Orange
  Brown
  Purple
}

enum Housing{
  0: "No housing"
  1: "1 House"
  2: "2 Houses"
  3: "3 Houses"
  4: "4 Houses"
  5: "Hotel"
}

Class Die{
  faceValue

}                                    
      
' Generalisation of a property
Class Square{
 name
 position
}

'Pawn "0..1" - "1" Square : is positioned on >

' specialisation of Property
Class Lot{}
Class Railroad{}
Class Utility{}

' other special(isation) Square
Class Go{
    salary
}

'We moeten de Outcome opsplitsen in een Spel specifiek gedeelte, en een gedeelte met info over speler resultaat

Class GameOutcome{
  timeStamp
  winner:String
  duration
  numberOfRounds:int
}

Class PlayerOutcome{
  playerName:String
  finalAmountOfMoney:float      
}

Class Account{
  name:String
  /numberOfWins
  /ELO-rating
}


MonopolyApplication "1" .. "0..1" GameSession : launches >
MonopolyApplication "1" .l. "*" Account : keeps track of >
GameSession "1" -- "2" Die : played with > 
GameSession "1" -- "1" Board : played on >
Player "2..8" -r- "*" GameSession : plays >
(Player, GameSession) .. PlayerOutcome  
GameSession "1" -- "0..1" GameOutcome : records >
Player "1" -u- "*" Turn : takes >
Die -[hidden]- Board
Board "1" -- "40" Square : consists of >
Player "1" -- "1" Pawn : plays with >
Pawn "0..8" -- "1" Square: lands >
Player "0..1" -- "*" Property : owns >
Lot -u-|> Property
Railroad -u-|> Property
Utility -u-|> Property
Property -u-|> Square
Go -r-|> Square
IncomeTax -l-|> Square

@enduml
```

## Step 4B - A minimalistic version  

``` plantuml
@startuml
skinparam style strictuml

Class Outcome{
  /totalWealth:float
  /playerName:String
  /endDate:Date
}
                                   
Class Game{
  main()
  startDate
  endDate
}                                    
                                   
Class Player{
 name
 money
}                               

Class Board{}
                                       

Class Pawn{
 /position
}                                    
                      
Class Property{
 color:String 
 housing:String
 /rent
 price
}

' Generalisation of a property
Class Square{
 name
 position
}

Game "1" -l- "2" Die : played with > 
Game "1" -r- "1" Board : played on >
Board "1" -d- "40" Square : consists of >
Player "2..8" -u- Game : plays >
Player "1" -d- "0..1" Outcome : scores >
Player "1" -r- "1" Pawn : plays with >
Pawn "0..8" -r- "1" Square: landed on >
Player "0..1" -d- "*" Property : owns >
Property -u-|> Square
@enduml
```


# Java Code

#    

