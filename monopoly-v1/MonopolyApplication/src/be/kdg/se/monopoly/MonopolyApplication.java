package be.kdg.se.monopoly;

import be.kdg.se.monopoly.domain.Account;
import be.kdg.se.monopoly.domain.GameSession;

import java.util.ArrayList;

class MonopolyApplication {
    //primitive attributes

    //conceptual attributes
    /**
     * NOTE : since we don't create an instance of this class,
     *          We don't need attributes in this class
     * The dependencies are generated on the fly when needed
     */

    public static void main(String[] args) {
        while(true){
            switch (args[0]){
                case "n":
                    //MonopolyApplication "1" .. "0..1" GameSession : launches >
                    GameSession game = new GameSession();
                    game.play();
                case "s":
                    //MonopolyApplication "1" .l. "*" Account : keeps track of >
                    Account account = new Account();
                    account.getName();
                    account.getNumberOfWins();
                    account.getELORating();

                case "x":
                    return;
            }
        }
    }
}


                                   


                                   

                               

                                 








