package be.kdg.se.monopoly.domain;

public class Property extends Square {
    //primitive attributes
    private float rentValue;
    private float price;

    //conceptual attributes
    private Color streetColor;
    private Housing housing;


    //Calculate rentValue based on a table of number of Houses
    public float getRentValue() {
        return rentValue;
    }
}


class Lot extends Property {
}

class Railroad extends Property {
}

class Utility extends Property {
}