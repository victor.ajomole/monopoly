package be.kdg.se.monopoly.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class GameSession {
    //primitive attributes
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private float maxTurnDuration; // in seconds ;

    //conceptual attributes

    private Die[] dice; //GameSession "1" -- "2" Die : played with >

    private Board board;//GameSession "1" -- "1" Board : played on >

    private ArrayList<Player> players;     //Player "2..8" -r- "*" GameSession : plays >

    private GameOutcome gameOutcome; //GameSession "1" -- "0..1" GameOutcome : records >

    public void play() {
        boolean finished = false;
        while (!finished) { //until game is over
            for (Player player : players) {
                player.takeTurn();
            }
        }
        for (Player player : players) {
            player.getPlayerOutcome();
        }
    }
}

