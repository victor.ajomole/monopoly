package be.kdg.se.monopoly.domain;

import java.util.ArrayList;

public class Square {
    //primitive attributes
    private String name;
    private int position;

    //NOTE : here a bi-directional association could be helpful
    private ArrayList<Pawn> landedPawns; //  Pawn "0..8" -r- "1" Square: landed on >

    public int getPosition() {
        return position;
    }
}


// other special(isation) Square
class Go extends Square {
}

class IncomeTax extends Square {
}
