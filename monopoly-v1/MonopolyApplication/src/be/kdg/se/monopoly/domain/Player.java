package be.kdg.se.monopoly.domain;

import java.util.ArrayList;

public class Player {
    //primitive attributes
    private String name;
    private float money;

    //conceptual attributes
    private ArrayList<Turn> turns; //Player "1" -u- "*" Turn : takes >
    private PlayerOutcome playerOutcome; // --SIMPLIFICATION of Domain Model -- (Player, GameSession) .. PlayerOutcome
    private Pawn pawn; //Player "1" -- "1" Pawn : plays with >
    private ArrayList<Property> ownedProperties; // Player "0..1" -- "*" Property : owns >

    public void takeTurn() {
    }

    public PlayerOutcome getPlayerOutcome() {
        playerOutcome.playerName=name;
        playerOutcome.finaleAmountOfMoney=money;
        return playerOutcome;
    }
}

