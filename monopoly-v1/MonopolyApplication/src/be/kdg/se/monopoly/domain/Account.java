package be.kdg.se.monopoly.domain;

public class Account {
    //primitive attributes
    private String name;

    //derived attributes
    public int getNumberOfWins() {
        return -1;
    }

    public float getELORating() {
        return -1;
    }

    //getters and setters
    public String getName() {
        return name;
    }
}
