# Monopoly

This project contains all necessary input and output for a minimal analysis for a Monopoly Game. 
It consists of a 
- [Data Analysis](https://gitlab.com/kdg-ti/software-engineering-1/monopoly/-/blob/main/monopoly-v1/MonopolyApplication/analysis/DataAnalysis/data-analysis-EN.md)
- [Functional Analysis](https://gitlab.com/kdg-ti/software-engineering-1/monopoly/-/blob/main/MonopolyApplication/analysis/FunctionalAnalysis/functional-analysis.md) and
- Business Analysis -- todo


## Disclaimer
This project is not a demonstration of a running game. 
