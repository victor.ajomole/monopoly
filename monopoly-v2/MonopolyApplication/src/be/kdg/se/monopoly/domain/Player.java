package be.kdg.se.monopoly.domain;

import java.util.ArrayList;

public class Player {
    //primitive attributes
    private String name;
    private float money;


    //conceptual attributes
    private Die[] dice = {new Die(), new Die()}; //    Player "1" -l- "2" Die : holds >
    private Cup cup; //    Player "1" -l- "1" Cup : played with >
    private Score score; //    Player "1" -d- "1" Score : scores >
    private Pawn pawn; //    Player "1" -r- "1" Pawn : plays with >
    private ArrayList<Property> ownedProperties; //    Player "0..1" -d- "*" Property : owns >

    public void takeTurn() {
        int totalValue = 0;
        for (int i = 0; i <2; i++) {
            dice[i].roll();
            int fv = dice[i].getFaceValue();
            totalValue += fv;
        }
    }

    public Die[] getDice() {
        return dice;
    }
}

