package be.kdg.se.monopoly.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class GameSession {
    //primitive attributes
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    //conceptual attributes

    private Die[] dice = {new Die(), new Die()}; //    Player "1" -l- "2" Die : holds >
    private Board board;//    Game "1" -r- "1" Board : played on >

    private ArrayList<Round> rounds;//    Game "1" -u- "*" Round

    private ArrayList<Player> players;     //Player "2..8" -u- Game : plays >

    public void playBad() {
        int roundCnt = 0;
        while (roundCnt < 10) {
            Round round = new Round(roundCnt,players);
            playRound(roundCnt);
        }
    }

    public void playRound(int round){
        for (Player p : players) {
            Die[] die = p.getDice();
            rollDice(die);
        }

    }

    private void rollDice(Die[] die) {
        int totalValue = 0;
        for (int i = 0; i <2; i++) {
            die[i].roll();
            int fv = die[i].getFaceValue();
            totalValue += fv;
        }

    }

    public void playGood() {
        rounds = new ArrayList<>(10);
        int roundCnt = 0;
        while (roundCnt < rounds.size()) {
            Round currentRound =new Round(roundCnt,players);
            rounds.add(currentRound);
            currentRound.play();
        }
    }
}

