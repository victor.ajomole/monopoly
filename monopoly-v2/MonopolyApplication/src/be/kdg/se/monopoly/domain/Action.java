package be.kdg.se.monopoly.domain;

public class Action {
    private ActionType type;
    private String log;


    @Override
    public String toString() {
        return "Action{" +
                "type=" + type +
                ", log='" + log + '\'' +
                '}';
    }
}

enum ActionType{
    ROLL, PAY_RENT, BUY_PROPERTY,BUY_HOUSE, MORTGAGE_PROPERTY;
}


