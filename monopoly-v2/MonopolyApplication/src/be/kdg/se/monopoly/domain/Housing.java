package be.kdg.se.monopoly.domain;

enum Housing {
    NO_HOUSING("No housing", 0),
    HOUSE_1("1 House", 1),
    HOUSE_2("2 Houses", 2),
    HOUSE_3("3 Houses", 3),
    HOUSE_4("4 Houses", 4),
    HOTEL("Hotel", 5);

    private final String key;
    private final Integer value;

    Housing(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}