package be.kdg.se.monopoly.domain;

import java.util.Date;

public class Score {
    private float amountOfMoney;
    private float[] valueOfOwnedProperties;
    private float totalWealth;
    private String playerName;
    private String[] trace;
    private float[] turnDurations;
    private Date endDate;
}