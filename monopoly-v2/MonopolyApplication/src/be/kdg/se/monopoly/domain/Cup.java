package be.kdg.se.monopoly.domain;

public class Cup {
    private int value;

    //conceptual   attributes
    private Die[] dice = {new Die(), new Die()}; //    Cup "1" -l- "2" Die : holds >

    public void roll() {
        value = 0;
        for (int i = 0; i < dice.length; i++) {
            dice[i].roll();
            value += dice[i].getFaceValue();
        }
    }

    public int getValue() {
        return value;
    }

}
