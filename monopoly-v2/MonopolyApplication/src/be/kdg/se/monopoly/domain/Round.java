package be.kdg.se.monopoly.domain;

import java.util.ArrayList;

public class Round {
    //conceptual attributes
    private ArrayList<Turn> turns; //Round "1" -l- "0..8" Turn
    private int counter;
    private ArrayList<Player> players;

    public Round(int roundCnt, ArrayList<Player> players) {
        this.counter = roundCnt;
        this.players = players;
    }

    public void play() {
        for (Player p : players) {
            p.takeTurn();
        }
    }
}
